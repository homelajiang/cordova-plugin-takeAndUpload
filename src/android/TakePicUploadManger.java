package com.sdr.takepicplugin;

import android.content.Intent;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by homelajiang on 2016/9/9 0009.
 */
public class TakePicUploadManger extends CordovaPlugin {
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        if (action.equals("start")) {
            Intent intent = new Intent(cordova.getActivity(), TakePicUploadActivity.class);

            intent.putExtra("userId", args.getString(0));
            intent.putExtra("accessToken", args.getString(1));
            intent.putExtra("serverUri", args.getString(2));
            intent.putExtra("status", args.getString(3));
            intent.putExtra("title",args.getString(4));

            cordova.getActivity().startActivity(intent);
            return true;
        }
        if (action.equals("test")) {
            Toast.makeText(cordova.getActivity(), args.getString(0), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}
