package com.sdr.takepicplugin;

import android.net.Uri;

/**
 * Created by homelajiang on 2016/9/21 0021.
 */

public class UploadFile {
    Uri uri;
    String type;

    UploadFile() {
    }

    UploadFile(Uri uri, String type) {
        this.uri = uri;
        this.type = type;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
